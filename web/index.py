import pandas as pd
import numpy
import pickle
import matplotlib.pyplot as plt

from flask import Flask
from flask import render_template, request, url_for
from sklearn.feature_extraction.text import TfidfVectorizer

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    return render_template('home.html')

@app.route("/test", methods=["GET"])
def test():
	return render_template('test.html')

@app.route("/result", methods=['POST'])
def result():
	temp = request.files.getlist('inputfile')
	df = pd.read_csv(temp[0])
	datas = df['Baris'].values
	datas = [item for item in datas if not isinstance(item, int)]
	result_NB, percentage_NB = classification_NB(datas);
	result_ME, percentage_ME = classification_ME(datas);
	return render_template('result.html', fname = temp[0].filename, result_NB=result_NB, result_ME=result_ME, percentage_NB=percentage_NB, percentage_ME=percentage_ME)

def percentage(part, total):
	return 100*float(part)/float(total)

def classification_NB(datas):
	af = 0
	ms = 0
	other = 0
	af_lines = []
	ms_lines = []
	other_lines = []
	count = []

	tfidf = pd.read_pickle("tfidf.pickle")
	features = tfidf.transform(datas)

	model = pd.read_pickle("cvMultinomialNaiveBayes.pickle")
	prediction = model.predict(features)

	for x in range(prediction.size):
		if prediction[x] == 0 :
			af+=1
		elif prediction[x] == 1 :
			ms+=1
		elif prediction[x] == 2 :
			other+=1

	total = af+ms+other
	jumlah_data_tiap_aliran = [af,ms,other]
	percentase_tiap_aliran = [percentage(af,total),percentage(ms,total),percentage(other,total)]
	for y in range(0,3):
		percentase_tiap_aliran[y] = float("{:.2f}".format(percentase_tiap_aliran[y]))

	return jumlah_data_tiap_aliran, percentase_tiap_aliran

def classification_ME(datas):
	af = 0
	ms = 0
	other = 0
	af_lines = []
	ms_lines = []
	other_lines = []
	count = []

	tfidf = pd.read_pickle("tfidf.pickle")
	features = tfidf.transform(datas)

	model = pd.read_pickle("cvMaxEntModel.pickle")
	prediction = model.predict(features)

	for x in range(prediction.size):
		if prediction[x] == 0 :
			af+=1
		elif prediction[x] ==1 :
			ms+=1
		elif prediction[x] ==2 :
			other+=1

	total = af+ms+other
	jumlah_data_tiap_aliran = [af,ms,other]
	percentase_tiap_aliran = [percentage(af,total),percentage(ms,total),percentage(other,total)]
	for y in range(0,3):
		percentase_tiap_aliran[y] = float("{:.2f}".format(percentase_tiap_aliran[y]))

	return jumlah_data_tiap_aliran, percentase_tiap_aliran