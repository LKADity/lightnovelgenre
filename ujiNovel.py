import pandas as pd
import numpy
import pickle
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.model_selection import train_test_split, KFold, cross_val_score, StratifiedKFold
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

from statistics import mean
from time import time

train_filename = 'dataset.csv'
# test_filename = 'Masahiro and Yuri.csv'
test_filename = 'TToaGAbhCaDttBotAtMtDK.csv'
model = MultinomialNB()
# model = LogisticRegression(random_state=0)

df1 = pd.read_csv(train_filename)
train_datas = df1['Baris'].values
train_datas = [item for item in train_datas if not isinstance(item, int)]
train_labels = df1['Genre']

df2 = pd.read_csv(test_filename)
test_datas = df2['Baris'].values
test_datas = [item for item in test_datas if not isinstance(item, int)]
test_labels = df2['Genre']

tfidf = TfidfVectorizer(sublinear_tf=True, min_df=0, norm='l2', encoding='utf-8', ngram_range=(1, 2))
train_features = tfidf.fit_transform(train_datas).toarray()

count_vect = CountVectorizer()
# test_features = count_vect.transform(test_datas[0]).toarray()

model.fit(train_features, train_labels)
# prediction = model.predict(test_datas)

# conf_mat = confusion_matrix(train_labels, prediction)
# fig, ax = plt.subplots(figsize=(3,3))
# sns.heatmap(conf_mat, annot=True, fmt='d')
# plt.ylabel('Actual')
# plt.xlabel('Predicted')
# plt.show()

print(model.predict(count_vect.transform(["This company refuses to provide me verification and validation of debt per my right under the FDCPA. I do not believe this debt is mine."])))