import pandas as pd
import numpy
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
	
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score


models = [
    MultinomialNB(),
    LogisticRegression(random_state=0),
]

df = pd.read_csv('train2.csv')
datas = df['Baris'].values
datas = [item for item in datas if not isinstance(item, int)]
labels = df['Genre']

tfidf = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='utf-8', ngram_range=(1, 2), stop_words='english')

# features = tfidf.fit_transform(df.Genre).toarray()
features = tfidf.fit_transform(datas).toarray()
print(features.shape)

X_train, X_test, y_train, y_test = train_test_split(datas, labels, random_state = 0)
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(X_train)
tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
clf = MultinomialNB().fit(X_train_tfidf, y_train)

CV = 5
cv_df = pd.DataFrame(index=range(CV*len(models)))
entries = []
for model in models:
	model_name = model.__class__.__name__
	accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=CV)
	for fold_idx, accuracy in enumerate(accuracies):
		entries.append((model_name, fold_idx, accuracy))
cv_df = pd.DataFrame(entries, columns=['model_name','fold_idx','accuracy'])

sns.boxplot(x='model_name', y='accuracy', data=cv_df)
sns.stripplot(x='model_name', y='accuracy', data=cv_df, 
              size=8, jitter=True, edgecolor="gray", linewidth=2)
# plt.show()
print(cv_df.groupby('model_name').accuracy.mean())
