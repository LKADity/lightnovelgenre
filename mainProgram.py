import pandas as pd
import numpy
import pickle
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split, KFold, cross_val_score, StratifiedKFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

from statistics import mean
from time import time

filename = 'dataset.csv'
model = MultinomialNB()
# model = LogisticRegression(random_state=0)

df = pd.read_csv(filename)
datas = df['Baris'].values
datas = [item for item in datas if not isinstance(item, int)]
labels = df['Genre']

tfidf = TfidfVectorizer(sublinear_tf=True, min_df=0, norm='l2', encoding='utf-8', ngram_range=(1, 2))
features = tfidf.fit_transform(datas).toarray()

# X_train, X_test, y_train, y_test, indices_train, indices_test = train_test_split(features, labels, df.index, test_size=0.33, random_state=0)
# model.fit(X_train, y_train)
# y_pred = model.predict(X_test)

accuracy_kfold = []
confusion_matrix_kfold = []
stratified = StratifiedKFold(n_splits = 10)
train_time = 0
test_time = 0

for(train,test) in (stratified.split(features, labels)):
	train_time_start = time()
	model.fit(features[train], labels[train])
	train_duration = time() - train_time_start
	train_time = train_time + train_duration
	test_time_start = time()
	predicted = model.predict(features[test])
	test_duration = time() - test_time_start
	test_time = test_time + test_duration
	accuracy_score = numpy.sum(predicted == labels[test]) / labels[test].shape[0]
	accuracy_kfold.append(accuracy_score)
	confusion_matrix_kfold.append(confusion_matrix(labels[test], predicted))
confusion_matrix_all = numpy.zeros((3,3), dtype=int)
for index in range(0,(len(confusion_matrix_kfold))):
	confusion_matrix_all = numpy.add(confusion_matrix_all, confusion_matrix_kfold[index])
print("Confusion Matrix: \n", confusion_matrix_all)
print("Accurary cross-val : ",accuracy_kfold)
average_accuracy = mean(accuracy_kfold)
print("Average accurary : ",average_accuracy)
print("train time: %0.3fs" % train_time)
print("test time: %0.3fs" % test_time)
# pickle.dump(model,open("cvMultinomialNaiveBayes.pickle", "wb"))
# pickle.dump(model,open("cvMaxEntModel.pickle", "wb"))