import pandas as pd
import numpy
import pickle
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split, KFold, cross_val_score, StratifiedKFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

from statistics import mean
from time import time

filename = 'dataset.csv'

df = pd.read_csv(filename)
datas = df['Baris'].values
datas = [item for item in datas if not isinstance(item, int)]
labels = df['Genre']

tfidf_vect = TfidfVectorizer(sublinear_tf=True, min_df=0, norm='l2', encoding='utf-8', ngram_range=(1, 2))
tfidf = tfidf_vect.fit(datas)
pickle.dump(tfidf,open("tfidf.pickle", "wb"))

filename = 'MaY.csv'
df = pd.read_csv(filename)
datas = df['Baris'].values
datas = [item for item in datas if not isinstance(item, int)]
labels = df['Genre']
features = tfidf.transform(datas)

af = 0
ms = 0
other = 0
af_lines = []
ms_lines = []
other_lines = []
count = []

model = pd.read_pickle("cvMultinomialNaiveBayes.pickle")
# model = pd.read_pickle("cvMaxEntModel.pickle")
prediction = model.predict(features)

for x in range(prediction.size):
	if prediction[x] == 0 :
		af+=1
		af_lines.append(datas[x])
	elif prediction[x] == 1 :
		ms+=1
		ms_lines.append(datas[x])
	elif prediction[x] == 2 :
		other+=1
		other_lines.append(datas[x])

print(af)
print(ms)
print(other)