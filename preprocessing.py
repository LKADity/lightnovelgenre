import re,csv,string
from nltk.corpus import stopwords
from nltk import word_tokenize 

stop_words = set(stopwords.words('english')) 
filenames = [
	# 'The companions I trusted in were all assassins planted by corrupt Heroes',
	
	# 'Onee-chan Will Become a Hero and Save the World',
	# 'Its Because You Said There Would Be Candy',
	# 'My Hero',
	# 'That One Time the Essay a Child Wrote over Summer Break was Way too Fantasy',
	
	# 'About the reckless girl who kept challenging a reborn man',
	# 'There is a narrative trick',
	# 'Masahiro and Yuri',

	# 'Wishing Happiness unto an Idiot Sister',
	# 'Garden of Avalon',
	# 'The Little Macho Girl',
	# 'The math problems of a relative elementary school kid are too weird',
	# 'There is an Angel in that Church'

	# 'All You Need Is Kill'
	# 'Katahane no Riku',
	# 'Little Princess in Fairy Forest',
	# 'Tada Sore Dake de Yokattan Desu',
	# 'Train Man',
	# 'You Shine in the Moonlit Night'
	# 'There is no Secret Organization'
	# 'The Tale of a Girl Abandoned by her Companion and Dropped to the Bottom of the Abyss to Meet the Demon King'
]

def file_len(fname):
    with open(fname, 'r',errors = 'replace', encoding='utf-8') as f:
        for i, l in enumerate(f):
            pass
    f.close()
    return i + 1

def clean_lines(lines):
	# return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", lines).split()) 	# REGEX Exclude special Characters
	temp = ' '.join(re.sub("(@[A-Za-z]+)|([^A-Za-z \t])|(\w+:\/\/\S+)", " ", lines).split())			# REGEX Include letter only
	return temp.lower()																					# lowercase for word matching

for x in range(0,len(filenames)):
	with open(filenames[x]+'.txt', 'r',errors = 'replace', encoding='utf-8') as fp:						# encoding=''utf-8 for txt file
		num_lines = file_len(filenames[x]+'.txt')
		with open(filenames[x]+'.csv', 'w', encoding='utf-8', newline='') as out_file:
			for y in range(0,num_lines):
				line = fp.readline()
				if line != "" and line != "\n" :
					line = clean_lines(line)
					word_tokens = word_tokenize(line)
					filtered_line = [w for w in word_tokens if not w in stop_words]
					writer = csv.writer(out_file)
					writer.writerow([filtered_line])
