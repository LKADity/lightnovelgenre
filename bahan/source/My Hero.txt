﻿I have a childhood friend.
That friend became the hero and saved the world.

We were born in a small village. We were the only ones born that year, our houses were next to each other, our mothers and fathers were close friends, and we were both boys, so we became friends as if it were only natural.
It would be easy to summarize with the words ‘we were always together’. Babysat together, ate together, took our baths together and were put to sleep together.
And we played together, caught colds together, fell over together, cried together, and laughed together.
That was what was natural, I didn’t have any particular complaints. We would often fight, but we got along well enough. We were close friends, after all.
But as we aged, I came to find out. Unlike me, who didn’t have any particularly special traits, that guy was amazing. His face was pretty, and since he grew up tall, it wasn’t just the village girls, he was even popular with the girls in the town over. When it came to books, he just had to read through them once to memorize them, he was quick on his feet, and he was good at hunting as well. No shit he’d be popular.
It was a bit irritating, but he’s a good guy, and I’m who I am. I liked him enough to get to thinking of it that way, and we stayed friends the whole time.
We did stupid things, made the adults angry, were put to cleaning the toilets together, complained it was a pain and went off to play, only to make them angry again. When we were kicked out to sleep in the shed, we made a ruckus all the way through the night, received the iron fist to the head, and grew older together.
In the far, far capital, I heard the evil spirits were doing bad things all over the place, but our village was out in the sticks, brimming with nature (in fact, that’s all we had), so we still had the blessings of the fae and it was completely someone else’s business.

Anyways, with this and that, we were fifteen, out shopping in town as per usual, and we were surprised. In that town that had so many people our knees gave way the first time we stopped by, there was such a crowd gathered, it made me liken the town’s normal state to our own village.
Of all things, it seems some VIP from the capital had come to search out the hero. The one who could pull the legendary sword from its scabbard was the hero, and for those with some confidence in their mettle, regardless of status, age or gender, they said they wanted them to challenge the sword without question.
Me and my childhood friend went to have a bit of a go, if you become the hero, I’ll pick you those nuts from the reaaally hard to reach place in the forest, or so we had some stupid pokes at one another as we got in line.

My turn came around, and naturally unable to draw that needlessly heavy sword, I felt just a bit relieved I didn’t speak out about my slight hopes before my hand reached the hilt. Not a snowball’s chance in ‘ell, I gave a grand laugh as I handed off the sword to my friend.
And the sword I had suspected to be welded shut, with the scabbard still in my grasp, I saw the sword alone slide out and settle itself in my childhood friend’s hands.

Those cheers and jeers that had been so noisy died out, and while everyone’s eyes gazed at my friend with looks of shock, my childhood friend was more surprised than any of them. In his pretty eyes, for some reason, it wasn’t the sword that he had just pulled out, but the stupid face I was making that was reflected. Even now I can remember it well.

After that, up and down, left and right, front and back, it was huge news.
With the hero’s appearance, the world was all topsy-turvy in thunderous applause. It was the sort of great ruckus where the king and prince, and even the princess came out.
They wanted him to defeat the lord of the spirits, they wanted him to go on a journey for that sake. My childhood friend tacked on a condition before he accepted.
He wanted me to accompany him on that journey. That was all.
Since he was at it, he coulda just asked for enough money to play around the rest of his life, but that’s all he asked for.
To add onto that, his parents also asked for it. To add onto that addition, my parents kicked me out and said I definitely had to follow him. To add on even further, everyone in the village, and the king who had made a dubious face at first, and the vassals, and the magicians, and the swordsmen, at this point, everyone in eye’s reach asked for it.

There was only one reason.
My childhood friend was the greatest failure in human history when it came to a sense of directions.

When you think he’s walking behind you, he disappears, when he’s walking in front of you, he’s gone when you blink, when he’s walking beside you, he’s not there the moment you laugh and reach to pat his shoulder. What sort of horror is this?
Even in the village we were born and raised, he can’t return home alone. The moment you take your eyes off of him, he disappears into the forest, disappears into the bog, disappears into the mountains, and even disappears into the village chief’s house. If he goes to the bathroom, he doesn’t return. If he goes to take a bath, he’s gone. To add onto that, he can’t make it to either of those places alone.
Even when he’s so popular, that’s the reason he can never keep a girl long. First off, he can’t make it to the meeting place. Even if he does, he disappears. He vanishes even if you’re holding hands, so what am I supposed to do? I’d end up dropping him off at date spots, and they’d treat me as a hindrance and spread strange rumors all over. It brings tears to a man’s eyes.
Finding my childhood friend was always my job. We were like brothers, so I can’t deny this feeling that the role was just shoved onto me, but I also wanted to play with him, so yeah, I searched as hard as I could.
His lack of directional sense only grew worse by the year, and as I searched for my childhood friend who wouldn’t return for three days if played poorly, I grew some confidence in my needlessly trained walking abilities.
Along the way, I ended up being the only one who could find him. My childhood friend was quick on his feet, and even if you just saw his back round the corner, he was no longer on the other side. In such a situation, the only one who could overtake him was me.
The robust knights, and the greatest magician in the country soon gave up on apprehending my childhood friend, so it ended up that I was to tag along on this spirit king extermination journey. It’s a right bother.

With this and that, we went on a journey to exterminate some source of trouble.
Before we left, the village got together to hold a banquet. With teary eyes, the village chief told my childhood friend, “You’re the pride of this village,” and, Do your best for the sake of the world.“ He also told me, “Well, yeah, do something,” and, “Try not to die.” Hey, village chief, sit down there for a second.

From the very beginning, our journey went through stormy seas. When the magician, the twin swordsmen, the prince, and the prince’s attendant were the only ones who were supposed to come along, the princess slipped her way onto the boat. With her long hair trimmed short, the princess who appeared from a barrel was quite the crazy woman, but I was chasing my childhood friend and ended up jumping into the sea after him, so I didn’t have the time to care.

As we excelled at racing around the mountains and forests, we had learned to use the hunting bow, the knife that would always prove useful, and the wood chopping hatchet, so our traveling companions decided to teach us the sword. We even learned from the princess. The princess was a sword master whose level the average knight could never hope to reach.
I got beaten up a lot.

A few days after we crossed the sea, I saw my first monster.
The stench let off from its peeling, decaying flesh, the bodily fluids dripping from it withered the flowers, the way it corrupted the soil was nothing but repulsive. I had to wonder whether something so repulsive could truly exist in this world, and the fact it was a living being was too much for me. I threw up.
Everything I learned was blown away, and when I had fallen, shaking on my ass, that guy stepped out front and waved the legendary sword.
He erased the monster with one swipe, our comrades sang praise of my childhood friend, as expected of a hero, they said. I spread sand over the spot I had vomited, swallowing my spit a number of times to cover up the sour taste in my mouth. And I tried to laugh. You really are something, as expected of the high and mighty hero, I’m proud of you, I wanted to say it all, but as if something was caught in my chest, I couldn’t say a thing.
I was no good at all; I shoulda just given a stupid laugh like I always did, but at that time, I couldn’t laugh no matter what I did. In that instant I looked down, not wanting anyone to see my face, the voices praising my childhood friend grew confused.
Right, he wasn’t there.
The hell!?

The twin swordsmen had their arms around his shoulders on both sides, laughing along with him, and yet he had gotten lost in the blink of an eye.
The search took three hours, and the moment I found him, while it was a little late, I realized it. Ah, this is no good. If I wasted time thinking I wasn’t needed, or that he could do it all, or that I was nothing to him, he would become lost.
By the way, after I found the lost boy, when I was leading him back to our comrades, my childhood friend became lost one more time. All despite the fact I was holding his hand so he couldn’t escape, and making sure not to take my eyes off of him. I’m going to stop blinking…
And childhood friend of mine, give back my seriousness.

In battle and even the journey itself, let alone useless I was even a hindrance. If you asked why I was here, I would answer lost child duty without hesitation. You could also say I had no time to think over the meaning of my existence or pride or any rubbish like that. I mean, when I used to search for him in familiar lands, now we were in a place where neither I nor he had any sense for the area.
When we’re walking he disappears, when we’re running, he disappears, when we’re laughing, he disappears, when we’re talking he disappears, when we’re eating, he disappears. Even when he’s sleeping in his PJs, he disappears.
And I would search for him. Walk and search, run and search, get angry and search, laugh and search, eat while I search, and rub my sleepy eyes as I search.
When I found him, I’d get in a fist and return him to our comrades. Again and again.
Within those days, as expected, I could only run away in battle and I was always apologizing to our comrades. When I did, our comrades made blank faces and tilted their heads, so I could only look blankly as well.
Our comrades told me they wouldn’t even be able to travel without me and laughed. My friend also laughed. I got in a good punch on him.

Along our journey, there was a time when I was abducted by a spirit.
If I wasn’t there, there was no one who would be able to bring him back, I got the feeling it said something like that. His lack of directional sense had even made its way to the spirits’ ears, I hung my head on that knowledge.
The spirits knew everything from his directional sense to the local specialty products of our homeland, but there are still some things they don’t know, I thought as, from behind the spirit beating me black and blue, I saw my childhood friend leap out making a face that turned even the spirit pale.
My childhood friend often disappears and often gets lost, he gets lost even if you bind his hands or carry him, but strangely, whenever I’m in a pinch, he always comes to save me. When I was surrounded by thugs, from nowhere in particular (a sewage pipe, for some reason. He stank) he appeared and drove off the thugs with his stench, and when I met with an avalanche, from out of nowhere (for some reason, inside the avalanche itself) he appeared and got stranded with me. But in the cave we found on the verge of death, we were only saved by each other’s heat. Even when I was attacked by a bear, or I fell from a cliff, my childhood friend would save me without fail. He would never abandon me, and he never once made a reluctant face when it came to saving me.
He would always make a fashionable entrance reaching out a hand and telling me to go.

Before he became the world’s hero, my childhood friend was my hero.

Well, when I gave a bashful laugh and tried to grab his hand, he would be lost, though!
At least give me a second!

Burned by the spirit’s flame, while the fire left in my body continued eating into me leading me along the boundary between life and death, I saw nothing but dreams of the past.
The magician’s desperate healing techniques showed results, and with my life fastened in place, the first thing I saw when my consciousness returned was my childhood friend gripping my hand in both hands, breaking down in tears. Apparently, he thought I was going to die. Yeah, I thought so too.
I was surprised to see the magician sobbing as well. When she kept declaring to me that useless men weren’t her type, she cried and cried saying she was glad I survived. Then she hit me. The fact she wouldn’t stay sweet to the end was, well, kinda cute.
It’s just that all the fingers of my right hand my childhood friend held tight were now broken. You bastard.

After many a twist and a turn, the troubles we faced upon reaching the spirit king’s palace were difficult to describe in words.
Passing through the gate, the hero got lost, at the fork, the hero got lost, when we turned to the wall, the hero got lost, before the four heavenly kings, the hero got lost. I managed to capture him when he was loitering around after he had taken the king’s head.
And thus the world was saved.

By the way, the troubadours changed that part into a wonderfully cool heroic tale. When I was lowering my head, apologizing for all the trouble we had caused them, the hero got lost. I found him and smacked him.

One way or another, we became heroes, and my childhood friend married the princess. I proposed to the magician.
I remained at the palace, receiving a position as his aid, and reminiscing longingly over the days where all we had to do was fight, I was chased around by work as I searched for a lost hero. Laughed at as the hero’s tag along, I searched for the hero. Feeling the itchy stares of the children who saw me as a hero myself with glimmering eyes, I searched for my dear friend. Chasing after my newly born child, I searched for my childhood friend.
My childhood friend had a child as well, but luckily, while he took after him quite a bit, the kid had no trace of his father’s lack of directions, and everyone pat their chest in relief. The one more relieved than anyone was my childhood friend; he was so relieved the power left his body, and he leaned his entire body weight onto me. I complained he was heavy as I tried to pat him on the back only to find he wasn’t there.
The bastard.

With this and that, we were having fun another day.
My childhood friend was lost again, and our comrades, the queen, and his men, and his son, they were all searching for him.
Even though I was already an old man, nothing had changed. I shouted Uoooooh! As I ran like the devil, spending my days searching for the hero.

I have a childhood friend.
My childhood friend became the hero, and saved the world while serving as my hero as well.

And today once more, my hero was in good health and lost.

 

 

I have a childhood friend.

From the moment I attained sentience, I could see it. I could see them. And I could hear them. The voices calling for me flooded me from all around. The hands beckoning for me sprouted from all I could see.

‘You will become our king.’
‘You should never have been born a human.’
‘You were never supposed to be a lifeform of that world.’

The voices, the hands, they would sway in front of me, beckoning me on.
By the time I noticed it, I was always alone. When up to a moment ago, I was supposed to be eating sweet snacks by a warm fireplace, by the time I noticed it, I was wandering the marshes barefoot. But my feet wouldn’t stop. Led on by whims of hundreds, thousands of hands, I continued walking with a hazy head.
I thought I had to return, but as if a bell was ringing out in my head, those reverberating voices buried up my thoughts. I have to return, the more I thought I had to go back, the more the voices would call.

‘That’s right, return.’
‘Return to the world where we live.’
‘Return to the place you’re supposed to be.’
‘It is only at that moment, that you will become you.’

Those voices, those hands, they covered up the sky. They shut off the wind. They made the ground below me disappear.
And I fell. I fell, I fell, I kept falling some more.

“Found you!”

The voices, the hands disperse.
I return to the world.
My childhood friend slid down into the marsh and hit me on the head.

“You idiot! We’re going home!”

He gripped my hand and led me away. My childhood friend born on the same day, at the same time, because he had come to find me in the bog, his shoes had become completely soaked. He never got angry over that, though he was eternally angry that I interrupted his snack time.
Whenever I was alone, my childhood friend would find me without fail. Even when I didn’t know where I was, he would definitely find me.
On a bad day, I would disappear again and again, but there was never a time where he didn’t look for me. Even if we fought, even when I said something terrible, my childhood friend would search and bring me back.
Once, I asked why he would search for me, and my childhood friend said, “Hah?” picking at his ear in low spirits.

“It’d be boring as hell without you.”

I see. So my childhood friend will be bored without me.
Then I really must go back. These obscure somethings were always following me around, and they were growing thicker year by year, but no matter what happened, I had to go back. That was the first time I held such a strong desire.

When my childhood friend tried jokingly to pull the hero’s sword, I ended up drawing it and became the hero.
I wasn’t scared of the monsters or spirits, but once we left those familiar lands and entered the domain of the spirits, those vague somethings only grew worse. There were times the hands covered up the world to such a degree I couldn’t see what was in front of me.
But for some reason, those hands never obstructed my childhood friend.
I could always see his form. I could always hear his voice.
That’s why I could return.

There was one time my friend was abducted by a spirit.
My childhood friend was going to die.
The moment I thought that, I could no longer hear a single sound in the world. Return, return, the resounding voices and the white, blank hands covering my world were all I could perceive.
At this rate I thought I wouldn’t be able to go back, but I wasn’t scared. At most, I thought of it as a bother. Afraid… the only time I was ever truly afraid was when I gripped the hand of my childhood friend, half his body burnt by a spirit’s flame, as I prayed for him to wake.
For some reason, I thought there would be no return if I ever let go of that hand. I didn’t know whether it was me or my friend who wasn’t coming back, but if nothing, I knew that I couldn’t let go, so I hung on for dear life itself.
In some part of me, I thought it couldn’t be helped if I went away. But my friend disappearing from the world… that alone was something I couldn’t bear.

I was told we had arrived at the spirit king’s castle, but I could no longer see a thing. Countless dozens of white hands sprouted, surging all around me like a wave.
By the time I noticed it, the king was before my eyes.
The spirit king spoke.
It told me I really wasn’t a human of this world. That the current spirit king was my replacement. I was supposed to be the spirit king, but various coincidence and happenstance overlapped, causing me to dwell in the stomach of my mother in the mountains.
I see, so from even before I was born, I wasn’t human. It felt strangely natural. The white hands clinging onto me, leading me, were residents of the world I was supposed to belong to, and in truth, I was supposed to destroy the world I was in now.
The spirit said it had come to destroy this world that wouldn’t give me back.
‘Return with me.’
The spirit said, giving a gentle smile and reaching out its hand. The white hands covering my entire field of vision shook with delight, and all the voices wailed out in madness.
The moment the spirit king’s hand and smile overlapped with that of my dear friend’s, I had lopped off its head.

By my slaying of their king, the hands and voices grew just a little reserved.
I had clearly indicated my will, or perhaps it was because the link that connected this world with that, the spirit king had been severed.
I properly fell in love and married the smiling girl whose face flushed red all the way to the nape of her neck fully exposed by her shortened hair. In the same year as my childhood friend, I had a kid.
While those unfamiliar days were bothersome at times, there was never a single thing I ever thought to throw away.
The hands and voices still called out to me. If I let my guard down for just a moment, I would find myself alone.
But I had already made my choice.
No matter how much they led me, no matter how much they covered the world, I would live as a human of this world, and I would die as one.
By the time I noticed it, I was standing atop a cliff on my own, but the voice that resounded was one of my friend calling for me, and the hand connected to it was a fist coming my way.
And so the sun sets on another day.

 

 

Lately, the voices and hands have grown fiercely in number.
I listen to the creaking of my chair as I quietly close my eyes.
Half a year ago, my childhood friend’s cold worsened and he left the world so easily. Watched over by his doting children, grandchildren, and great grandchildren, on a bed that’s sheets were changed every day and gave off a nice smell, he left with a peaceful look on his face.
My wife left the year before last. My great granddaughter was married last month.
I think I’ve done plenty.
The pure white hands covering my field of vision beckon me as they sway. The voices tell me there’s no longer any need for me to hesitate, echoing on and on in my head. I can’t see a single thing in this world anymore. I can’t hear a thing.
I can tell the hands are waiting to take me by the hand and take back my soul.

But I wasn’t too worried.
Even if I died now, and they beckoned to my soul, I get the feeling the hand reaching out for me won’t belong to them.

‘You idiot! Look, everyone’s waiting for you!’

My childhood friend who never stopped looking for me will shout as he lowers his fist on my head. I’m sure he’ll lead a ‘directional failure’ like me off to the same place everyone else has gone.

I have a childhood friend.
My childhood friend kept me in the world and saved me. He is my hero.